const { numberToWords, largeNumbers } = require('./static/numberToWords');

describe('Number to words', () => {
  test('$0', () => {
    expect(numberToWords('0')).toBe('ZERO DOLLARS');
  });

  test('$0000', () => {
    expect(numberToWords('0000')).toBe('ZERO DOLLARS');
  });

  test('$0.00', () => {
    expect(numberToWords('0.00')).toBe('ZERO DOLLARS AND ZERO CENTS');
  });

  test('$0.005', () => {
    expect(numberToWords('0.005')).toBe('ZERO DOLLARS AND ONE CENT');
  });

  test('$1', () => {
    expect(numberToWords('1')).toBe('ONE DOLLAR');
  });

  test('$2', () => {
    expect(numberToWords('2')).toBe('TWO DOLLARS');
  });

  test('$0.01', () => {
    expect(numberToWords('0.01')).toBe('ZERO DOLLARS AND ONE CENT');
  });

  test('$0.02', () => {
    expect(numberToWords('0.02')).toBe('ZERO DOLLARS AND TWO CENTS');
  });

  test('$0.1', () => {
    expect(numberToWords('0.1')).toBe('ZERO DOLLARS AND TEN CENTS');
  });

  test('$0.994', () => {
    expect(numberToWords('0.994')).toBe('ZERO DOLLARS AND NINETY NINE CENTS');
  });

  test('$0.995', () => {
    expect(numberToWords('0.995')).toBe('ONE DOLLAR AND ZERO CENTS');
  });

  test('$123.456', () => {
    expect(numberToWords('123.456')).toBe('ONE HUNDRED AND TWENTY THREE DOLLARS AND FORTY SIX CENTS');
  });

  test('-$1', () => {
    expect(numberToWords('-1')).toBe('NEGATIVE ONE DOLLAR');
  });

  test('-$2', () => {
    expect(numberToWords('-2')).toBe('NEGATIVE TWO DOLLARS');
  });

  test('$10', () => {
    expect(numberToWords('10')).toBe('TEN DOLLARS');
  });

  test('$100', () => {
    expect(numberToWords('100')).toBe('ONE HUNDRED DOLLARS');
  });

  test('$110', () => {
    expect(numberToWords('110')).toBe('ONE HUNDRED AND TEN DOLLARS');
  });

  test('$1000', () => {
    expect(numberToWords('1000')).toBe('ONE THOUSAND DOLLARS');
  });

  test('$1001', () => {
    expect(numberToWords('1001')).toBe('ONE THOUSAND AND ONE DOLLARS');
  });

  test('$1050', () => {
    expect(numberToWords('1050')).toBe('ONE THOUSAND AND FIFTY DOLLARS');
  });

  test('$1100', () => {
    expect(numberToWords('1100')).toBe('ONE THOUSAND, ONE HUNDRED DOLLARS');
  });

  test('$1110', () => {
    expect(numberToWords('1110')).toBe('ONE THOUSAND, ONE HUNDRED AND TEN DOLLARS');
  });

  test('$1111110', () => {
    expect(numberToWords('1111110')).toBe('ONE MILLION, ONE HUNDRED AND ELEVEN THOUSAND, ONE HUNDRED AND TEN DOLLARS');
  });

  test('Blank', () => {
    expect(numberToWords('')).toBe('Enter a dollar and cent amount.');
  });

  test('$1e2', () => {
    expect(numberToWords('1e2')).toBe('Scientific notation (e) is not supported.');
  });

  test('More digits than supported', () => {
    const maxLength = largeNumbers.length * 3;
    let digits = '';
    for (let i = 0; i <= maxLength; i++) {
      digits = digits + '9';
    }
    const expected = 'Maximum supported dollars are up to ' + maxLength + ' digits in length.'
    expect(numberToWords(digits)).toBe(expected);
  });
});