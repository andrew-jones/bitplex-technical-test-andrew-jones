# Bitplex Technical Test - Andrew Jones

This README explains how to run this project locally, see `documentation.md` in the same directory as this file for information about the project.

## Live site
The live site can be found at https://polar-tor-52159.herokuapp.com. It's important to note here that because it uses Heroku, and the 'database' is just a file (covered elsewhere in these docs), that Heroku may wipe files it temporarily stores. This means it's possible to register an account and come back at some point in the future, and your account may be gone, requiring you to register again.

## Running locally
This project was built on node.js version `14.16.1` and is the recommended version you should use, although recent versions should work. It was also built using Windows 10, however I think all the commands should be working fine on other operating systems, although that hasn't been tested.

To get started, clone this repo to your computer by running the command:  
`git clone https://gitlab.com/andrew-jones/bitplex-technical-test-andrew-jones.git`

Then you want to navigate to the folder and open a terminal and run `npm install` to install all the required dependencies. Next, if you just want to run the project locally run `npm start`. If however, you want to edit things and have the server automatically restart, and/or automatically build the SASS files, you should run `npm run dev` (note you'll still need to refresh the webpage manually unless you use something like Prepros).

Note that there is no need to set a database address or credentials, as the 'database' for this project is just a JSON file called db.json that is created in the root directory of this project at runtime, and sessions are also file based and are created in a sessions folder in the root directory of this project at runtime.

Once you have installed the dependencies and ran the desired command to start, the output in your terminal should tell you the address to access the server. It probably says `running on localhost:3000` unless you've changed the port in the code, or have set an environment variable called PORT that has changed this. So open a web browser at `localhost:3000` (changing the port if your output is different), and you should be presented with the front-end interface.

Since you're using a local version of a database, any login credentials you've used elsewhere won't exist, so you'll have to register a new account in order to log in and access the main application.

## Testing
If you make changes to the code and want to test the changes to make sure you haven't caused any bugs, you can run `npm run test` to run the tests found in `./src/test.js` to confirm that the tests still pass.
