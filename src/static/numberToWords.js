const DIGITS = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
const TEENS = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
const TENS = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
// add more to below from the wiki page later. but lets start with a reasonable amount
const LARGE_NUMBERS = ['hundred', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion', 'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'Unvigintillion', 'Duovigintillion', 'Tresvigintillion', 'Quattuorvigintillion', 'Quinvigintillion', 'Sesvigintillion', 'Septemvigintillion', 'Octovigintillion', 'Novemvigintillion', 'Trigintillion', 'Untrigintillion', 'Duotrigintillion', 'Trestrigintillion', 'Quattuortrigintillion', 'Quintrigintillion', 'Sestrigintillion', 'Septentrigintillion', 'Octotrigintillion', 'Noventrigintillion', 'Quadragintillion'];

/**
 * Converts a number to it's word format for numbers less than 20
 * @param {string} number A whole number, not a decimal
 * @returns {string} the word/s, or '' if empty.
 */
function sub20(number) {
  if (number.length > 2) {
    number = number.slice(number.length - 2, number.length);
  }

  // 0 - 9
  if (number >= 0 && number < 10) {
    // could be 05 or 5 so get last digit in string
    return DIGITS[number[number.length - 1]];
  }

  // 10 - 19
  if (number >= 10 && number < 20) {
    // get array index based on the second digit of the number
    return TEENS[number[1]];
  }

  return '';
}

/**
 * Converts a number to it's word format for numbers less than 100
 * @param {string} number A whole number, not a decimal
 * @returns {string} the word/s
 */
function sub100(number) {
  // we get the last two digits. eg if its 010 then we make it 10
  if (number.length > 2) {
    number = number.slice(-2);
  }

  if (number < 20) {
    return sub20(number);
  }

  // get tens based on first digit. minus two because starting at 20
  const ten = TENS[number[0] - 2];
  // if second digit is zero we dont want to say zero so we are done
  if (number[1] == 0) {
    return ten;
  }

  const single = DIGITS[number[1]];

  return ten + ' ' + single;
}

/**
 * Converts a number to it's word format for numbers less than 1000
 * @param {string} number A whole number, not a decimal
 * @returns {string} the word/s
 */
function sub1000(number) {
  if (number.length > 3) {
    number = number.slice(0, 3);
  }

  if (number < 20) {
    return sub20(number);
  }

  // 20 - 99
  if (number >= 20 && number < 100) {
    return sub100(number);
  }

  // 100 - 999
  if (number >= 100 && number < 1000) {
    // get the first numbers digit and return hundred after it
    const start = DIGITS[number[0]] + ' ' + LARGE_NUMBERS[0];

    // if last two digits are 0
    if (number.slice(1) == 0) {
      return start;
    }

    return start + ' and ' + sub100(number.slice(1));
  }

  return '';
}

/**
 * Converts a whole number to it's 'cents' word format
 * @param {string} number A whole number, not a decimal
 * @returns the word/s for the 
 */
function getCents(number) {
  if (number.length === 0) {
    return '';
  }

  // if 01 or 001 etc do this. if 1 was passed in, it would be 10 now
  if (number == 1) {
    return ' and one cent';
  }

  const cents = sub100(number);

  return ' and ' + cents + ' cents';
}

/**
 * We cant do number[index] = newDigit; because strings are immutable, so this essentially does that for us
 * @param {string} number 
 * @param {number} index 
 * @param {string} newDigit 
 * @returns a new string with the character at index changed to the newDigit
 */
function changeDigit(number, index, newDigit) {
  return number.substring(0, index) + newDigit + number.substring(index + 1);
}

/**
 * Round cents, and if cents are 995+, then we have to round dollars
 * @param {string} number Whole number with optiomal decimals
 * @returns the rounded number
 */
function roundNumber(number) {
  const split = number.split('.');
  let dollars = split[0];
  let decimal = split.length > 1 ? split[1] : '';

  // this changes '0000' to '0' etc. Stops it being treated as '0,000' later etc
  if (dollars == 0) {
    if (dollars.match(/^-/)) {
      dollars = '-0';
    } else {
      dollars = '0';
    }
  }

  // we want to change decimal to be at least 2 digits, because 0.1 is 10 not 1
  if (decimal.length == 1) {
    decimal = decimal + '0';
  }

  // if theres a third digit, and its 5 or above, we have to round
  if (decimal.length > 2 && decimal[2] >= 5) {
    // if the first two digits are 99, then we have to round up to a dollar
    if (decimal.slice(0, 2) == 99) {
      decimal = '00';
      // now we want to loop through the entire string, finding a suitable number to add 1 to
      // start from the end, looking for number to increase
      for (let i = dollars.length - 1; i >= 0; i--) {
        // if it equals 9 we change it to 0 and keep looking
        if (dollars[i] == 9) {
          dollars = changeDigit(dollars, i, '0');
          // if we hit the start and we've changed it to a 0, we need to add a 1 to the start
          // ie 0.995 is 1.00
          if (i === 0) {
            dollars = '1' + dollars;
          }
        } else {
          // otherwise increase it by 1 and done
          const newNumber = (parseInt(dollars[i]) + 1).toString();
          dollars = changeDigit(dollars, i, newNumber);
          break; // exit the for loop
        }
      }

    } else {
      // otherwise just round the decimal. only care about the first 3 places
      const epsilion = Number.EPSILON || 2.220446049250313e-16; // ie11 doesnt support EPSILON
      const asDecimal = parseFloat('0.' + decimal.slice(0, 3));
      const rounded = Math.round((asDecimal + epsilion) * 100);
      const roundedAsString = rounded.toString();
      decimal = roundedAsString;
    }
  } else {
    decimal = decimal.slice(0, 2);
  }

  return { dollars: dollars, decimal: decimal };
}

/**
 * Converts a number into groups of threes
 * @param {string} number A whole number
 * @returns array of groups. '1000000' => ['1', '000', '000']
 */
function splitDollarsIntoGroups(number) {
  // loop through all chars in the number backwards, and add a comma after every third
  let withCommas = '';
  for (let i = 0; i < number.length; i++) {
    withCommas += number[number.length - 1 - i];
    if ((i + 1) % 3 === 0) {
      withCommas += ','
    }
  }
  // the list will now in reverse order so reverse it to be the same.
  withCommas = withCommas.split("").reverse().join("");
  // if number was say "123" the string would be ",123" so lets cut off any comma from the start
  if (withCommas.match(/^[-]/)) {
    withCommas = withCommas.slice(1)
  }
  // now split it into groups of three based on commas
  const threes = withCommas.split(',');

  return threes;
}

/**
 * Converts each set of threes into their word versions, with large numbers
 * @param {string[]} threes Array of strings, that is a number split into threes, from splitDollarsIntoGroups(number)
 * @returns ['1', '111'] => ['one thousand', 'one hundred and eleven']
 */
function threesToWords(threes) {
  const words = []; // store each word in words array to join later
  // if 1,000,000 start at million, 1,000 start at thousand, etc
  let largeNumberStartIndex = threes.length - 1;
  for (let groupIndex = 0; groupIndex < threes.length; groupIndex++) {
    const group = threes[groupIndex];
    const word = sub1000(group);

    if (threes.length > 1 && group == 0) {
      // just skip zero if the number isn't exactly zero
    } else {
      // need to factor in hundred. dont add that to string if index is 0
      if (largeNumberStartIndex > 0) {
        words.push(word + ' ' + LARGE_NUMBERS[largeNumberStartIndex])
      } else {
        words.push(word); // dont want it to say one hundred if its just one. so dont add that in
      }
    }
    largeNumberStartIndex -= 1;
  }

  return words;
}

/**
 * 
 * @param {string[]} threes Array of strings, that is a number split into threes, from splitDollarsIntoGroups(number)
 * @param {string[]} words Array of strings, that is a number split into threes converted to words, from threesToWords(threes)
 * @param {string} cents String, result from getCents(number)
 * @param {boolean} isNegative Whether to add negative to the start or not
 * @returns Combines all the words and formats them together, giving the end result of dollars and cents
 */
function formatWords(threes, words, cents, isNegative) {

  if (words.length > 0 && words[0] === 'one dollar') {
    const oneDollar = 'one dollar' + cents;
    if (isNegative) {
      return ('negative ' + oneDollar).toUpperCase();
    }
    return oneDollar.toUpperCase();
  }

  let formatted = '';
  // if the numbers over a thousand, and the last group is less than one hundred, add an and to the
  // last one. but comma separate all the others
  if (threes.length > 1 && threes[threes.length - 1] < 100) {
    const allButLast = words.slice(0, words.length - 1).join(', ');
    const andLast = allButLast + ' and ' + words[words.length - 1];
    formatted = (andLast + ' dollars' + cents);
  } else {
    formatted = (words.join(', ').toString() + ' dollars' + cents);
  }

  // 1 followed by 0s, past 100, result in ' and x dollars'. So drop the and if thats the case
  if (formatted.match(/^\sand\s/)) {
    formatted = formatted.slice(5);
  }

  if (isNegative) {
    return ('negative ' + formatted).toUpperCase();
  }

  return formatted.toUpperCase();
}

/**
 * Converts a negative number to a positive, or leaves a positive as positive
 * @param {string} number the whole number with optional decimals to convert
 * @returns the positive number
 */
function getAbsoluteNumber(number) {
  if (number.match(/^[-]/)) {
    return number.substring(1);
  }

  return number;
}

/**
 * Runs checks against the provided number to see if it is valid
 * @param {string} number whole number with optional decimals to validate
 * @returns an object with an error if it failed, or true if it succeeded
 */
function isValidInput(number) {

  // note that the input.value from js is likely to be '' if its an invalid number like 123e
  // so most of these wont be called. but we still test for the other cases in case this is used
  // elsewhere (maybe in a cli version or something)

  // we want number to be a string and contain something
  if (typeof (number) !== "string") {
    return { error: 'Number must be of string type' }
  }

  if (number.length === 0) {
    return { error: 'Enter a dollar and cent amount.' };
  }

  // we arent supported scientific notation. 123e wont trigger this as its not a number,
  // but 123e1 will
  if (number.match(/e/g)) {
    return { error: 'Scientific notation (e) is not supported.' };
  }

  // the following shouldnt be run from client side, since they wont be valid numbers
  // but adding them in just in case

  if (number.match(/[-]/g) && number[0] !== '-') {
    return { error: 'Minus (-) can not be used other than at the start.' };
  }

  if (number.match(/[+]/g) && number[0] !== '+') {
    return { error: 'Plus (+) can not be used other than at the start.' };
  }

  if (number.match(/[,]/g)) {
    return { error: 'Commas (,) are not valid.' };
  }

  return true;
}

/**
 * When given a number in string format, this function will convert it to words and return the result
 * @param {string} number whole number with optional decimals
 * @returns the number as words, or a message to display to the user if something went wrong
 */
function numberToWords(number) {
  const valid = isValidInput(number);
  if (valid.error) {
    return valid.error;
  }

  const rounded = roundNumber(number);
  const dollars = rounded.dollars;
  const decimal = rounded.decimal;

  const cents = getCents(decimal);
  let absoluteDollars = getAbsoluteNumber(dollars);
  const isNegative = absoluteDollars != dollars;

  if (absoluteDollars == 1) {
    return formatWords([], ['one dollar'], cents, isNegative);
  }

  if (absoluteDollars.length > LARGE_NUMBERS.length * 3) {
    return 'Maximum supported dollars are up to ' + (LARGE_NUMBERS.length * 3) + ' digits in length.';
  }

  // now split it into groups of three based on commas
  const threes = splitDollarsIntoGroups(absoluteDollars);

  const words = threesToWords(threes);

  const formatted = formatWords(threes, words, cents, isNegative);

  // if something went wrong, and undefined is in the text, lets not show that to user
  // this shouldnt ever happen, but just in case
  if (formatted.match(/UNDEFINED/g)) {
    return 'There was an error converting the number.'
  }

  return formatted;
}

// export for testing etc
if (typeof exports !== 'undefined') {
  if (typeof module !== 'undefined' && module.exports) {
    const toExport = {
      numberToWords: numberToWords,
      largeNumbers: LARGE_NUMBERS
    };
    exports = module.exports = toExport;
  }
}