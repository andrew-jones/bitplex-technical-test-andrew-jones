const express = require('express');
const passport = require('passport');
const path = require('path');

const database = require('./database');
const helpers = require('./helpers');

const router = express.Router();

// middleware to check if user is logged in or not. used to secure routes we require being logged in
const loggedIn = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.redirect('/login');
  }
}

// users that arent logged in get redirected here if page requires being logged in
router.get('/login', async (req, res) => {
  res.render('auth', { login_error: req.flash('error') });
});

// login POST from form will hit this. if we authenticate it will redirect us
router.post('/login', async (req, res, next) => {

  passport.authenticate('local', (err, user, info) => {
    if (err) { return next(err); }

    if (!user) {
      return res.render('auth', { login_error: info.message, username: req.body.username });
    }

    req.logIn(user, function (err) {
      if (err) { return next(err); }
      return res.redirect('/');
    });

  })(req, res, next);
});

router.post('/register', async (req, res, next) => {
  const { username, password } = req.body;

  // do we have fields on body we want
  if (!username || !password) {
    return res.render('auth', { register_error: 'Username and password is required.', username });
  }

  // does user already exist
  const existingUser = await database.getUserByUsername(username);
  if (existingUser) {
    return res.render('auth', { register_error: 'That username is already taken.', username });
  }

  // register the user
  const hash = helpers.hashPassword(password);
  const user = await database.insertUser(username, hash);

  req.logIn(user, function (err) {
    if (err) { return next(err); }
    return res.redirect('/');
  });
});

router.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/login')
});

// the home page, if logged in
router.get('/', loggedIn, async (req, res) => {
  res.render('index', { username: req.user.username });
});

// static file server to get the css etc
router.use('/', express.static(path.join(__dirname, './static')));

module.exports = router;