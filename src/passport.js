const localStrategy = require('passport-local').Strategy;
const database = require('./database');
const { isSamePassword } = require('./helpers');

const local = new localStrategy({
  usernameField: 'username',
  passwordField: 'password'
}, async (username, password, done) => {
  const user = await database.getUserByUsername(username);
  // if (err) {}
  if (!user) {
    return done(null, false, { message: 'User with username not found' });
  }

  if (!isSamePassword(password, user.password)) {
    return done(null, false, { message: 'Password is incorrect' });
  }

  return done(null, user);
});

module.exports = (passport) => {
  // user to cookie
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });
  // id to user
  passport.deserializeUser(async (id, done) => {
    const user = await database.getUserById(id);
    const err = null;
    done(err, user);
  });
  // just using local for this
  passport.use(local);
};