const bcrypt = require('bcrypt');
const SALT_ROUNDS = 10;

module.exports = {
  /**
   * Used to hash and salt a password
   * @param {string} password Plaintext password
   * @returns Hashed + salted password
   */
  hashPassword: (password) => {
    return bcrypt.hashSync(password, SALT_ROUNDS);
  },

  /**
   * See if a password derives from the same password as a hash
   * @param {string} password Plaintext password
   * @param {string} hash An existing hashed password
   * @returns true if derive from the same password, false otherwise
   */
  isSamePassword: (password, hash) => {
    return bcrypt.compareSync(password, hash);
  }
}