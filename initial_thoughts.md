**This was essentially my thoughts before programming the solution, trying to understand the problem.**

https://englishlessonsbrighton.co.uk/saying-large-numbers-english/  
https://en.wikipedia.org/wiki/Names_of_large_numbers

This will assume we are using the short version of numbers (that Australia uses)
For example one billion would be 1,000,000,000, not 1,000,000,000,000

Also assume we are using British English for pronunciation, so we will have 'and' in the words

Figuring out what words we need:  
At least zero through nine  
ten, twenty, thirty, ... ninety  
Need eleven through nineteen  
Then should be each word to describe groups of large numbers: hundred, thousand, million, etc

Need to research if there's any others that are missing, but from what I can tell this should cover all cases

So thoughts for logic behind this:  
Let's ignore cents for the moment  
"We use different words to describe numbers depending on how many digits (numbers) they contain"  
So pre-100, we have single digits and teens, and then combination of tens and a single digit
Post-100, we have groups of three digits that contains it's 'large number' word, and then same rules as pre-100  
The large number is determined by how many groups of three there's been to the right

We then need to factor in not pronouncing zeros for a non-zero number. The logic seems to be if the group of 3 digits doesn't contain non-zeros for the second or third digit, then it's just the "large number" that is pronounced

Connecting all the words seems to be spaces between group of three, commas separating the groups, and then instead of the last set having a comma separation, it is finished with an and

Something else I've just noticed while saying words to myself is every group of three, is exactly the same as sub-999 numbers, but with the "large number" word thrown on the end
so 123 and 123,xxx is one hundred and twenty three, compared to one hundred and twenty three 
thousand

Cents is the same logic, although for this we round to the nearest two digits. But we can just
call this function again and it will work

psuedocode of how this should look  
 - separate dollars and cents  
 - find the highest 'large number' based on overall length
 - separate dollars into groups of three digits, from the right
 - then iterate over the groups from biggest, and get the sub-999 value, and throw on the large number
 - then keep shrinking large number until we hit the end
 - then
   - get cents by rounding to nearest two digits, then get sub-999 value
   - stich it all together and format