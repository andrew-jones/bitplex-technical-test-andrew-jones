# Technical Test Documentation

## Approach
### The stack
The backend for this project was written in Node.js. The frontend consists of templates written in Liquid, interactions written in Javascript, and styling written in SASS.

Neither the frontend or backend use Typescript, which was a choice to keep this project simple for building, and because the project is so small that typings won't matter as much.

I chose Node.js for the backend as it's the language I've used most as of late, and it's super easy to manage dependencies, run tests, etc. It's also very "plug and play" allowing me to just get modules to help build what I need without screwing around.

I decided to just stick with a basic templating language and Javascript to keep the project straight forward as well. It didn't seem like a good place to use React or another frontend library/framework for a project of this size. I also didn't see JQuery to needed since it's not doing serious DOM manipulation, so it's just plain Javascript. I had to change some code for IE11 to work (such as no string.startsWith(x) method), so I could have used Babel or Typescript, but again, that could complicate things and I know how to do them in vanilla JS anyway.

I chose SASS to generate the styling because it was super easy to setup, and allowed me to create CSS faster than manually writing it all, and have my styling better structured.

### Database
The database doesn't really exist. The only things it would be used for in this project is inserting and selecting users, and managing sessions. Instead I just opted to have a json file that contains the users and store them in an array in memory to easily access (and save the file when new user is created). Obviously this wouldn't scale great if you had hundreds of users, but for a proof of concept/test I think it proves the point, and saves having to muck around with database migrations, credentials, URLs, etc. Sessions are also saved in files under a sessions folder.

### Authentication
Authentication is super basic, and just uses Passport.js to help manage creating cookies and the likes for users. Passwords are hashed and salted using Bcrypt with 10 rounds. I decided against going with OAuth2, hosting an auth server, or using third party integrations to reduce complexity for this project. Simple and straight forward to do what it's intended to do.

### Word generation logic
The code for this logic can be found at `./src/static/numberToWords.js`, with the `numberToWords(number)` function near the bottom of the file being the starting point of the logic for converting the number.

If you want to see my thoughts prior to actually creating the logic, check out `initial_thoughts.md`.

To get started, I initially treat the numbers as a string so that it can theoretically be as long as needed, provided you know the names of large numbers. I didn't want to run into any overflow issues or anything, so strings it was.

Then I realised that if you group the numbers by three, the way you say all numbers is exactly the same as a sub-1000 number, and then just add the large number name onto it. An example is 123 and 123,xxx. The first is one hundred and twenty three, compared to one hundred and twenty three thousand. It doesn't matter how big the large number is, it's always going to be treated as a sub-1000 number with potentially a large number name thrown on the end.

The logic is something like this:  
1. Round the decimal (if it's present), and see if the whole number needs to increase
2. Get the postitive value of the number (aka remove the `-` symbol).
3. Handle cases such as zero, one dollar, etc. and possibly return early
4. Split string into whole and decimal
5. Group whole number into threes, starting from right most number
6. Loop over each set of three, with largest first  
  i. Generate words for it as if it was 999 or less  
  ii. Add the large number word to it  
  iii. Add the word to an array
7. Get the words for decimal if it exists
8. Format them all, throw an 'and' between the last and second last whole values if needed, and throw in a negative if the initial number was a negative

### SEO
There isn't any. I decided for this project not to spend time on it as it's not trying to rank well. I've also got a robots.txt to tell bots to not worry about it. You'll also notice I haven't set a favicon.

## Resources
### Learning
I used [this link](https://englishlessonsbrighton.co.uk/saying-large-numbers-english/) for learning a bit about how numbers are spoken, and what rules there are around them.

And I used [this link](https://en.wikipedia.org/wiki/Names_of_large_numbers) to get a list of the names of numbers once they reach a certain size.

Other than that I just said words to myself and tried to figure out some sort of pattern between them all.

### Other
I used [this link](https://stackoverflow.com/questions/39695614/browsersupport-of-number-epsilon) to get a rough number for Number.EPSILON since IE11 doesn't support it.

I also used Google and Stackoverflow to find random tiny snippets or names of functions that I regularly forget, such as the format for getting substrings etc.

### Node modules
For a full list of the modules I used you can check out the `package.json` file of this project. I'll cover a few of the useful ones and why I've used them.
#### Bcrypt
This handles the hashing and salting of passwords for me.
#### Express
Super easy to use, no fuss server, so that I can easily serve files and do the backend logic that I need to do.
#### Passport
Handles part of the auth for me, such as creating the cookies and helper functions to make implementation faster, but also give me enough control to do what I want auth wise.
#### Session File Store
This just writes sessions to a file instead of in memory or in a database.
#### Node SASS
This watches the *.scss files and compiles them into .css for me when changes are made.
#### Nodemon
This refreshes the server every time a change is made so I don't have to manually restart each time.
#### Jest
Simple and easy to use tests to make sure I get the expected outputs from functions etc.

### Google fonts
I made use of Google Fonts for the Roboto text that is used throughout, just to make it look a little bit nicer.

## Test plan
Automated tests run a bunch of different inputs to test the output of converting a number to words, to confirm that they output as expected. This covers $0, $0000, $0.00, $0.005, $1, $2, $0.01, $0.02, $0.1, $0.994, $0.995, $123.456, -$1, -$2, $10, $100, $110, $1000, $1001, $1050, $1100, $1110, $1111110, '', $1e2, and a number that is more digits than supported. This coveres all different cases I could think of that was worth checking for.

$0, $0000, $0.00 were used to cover the outputs of zeros.

$0.005, $0.994, $0.995, $123.456 were used to cover different rounding outputs.

$1, $2, -$1, -$2 were used to cover negative formatting, and make sure DOLLAR was shown when $1, but DOLLARS for $2.

$10, $100, $110, $1000, $1001, $1100, $1110, $1111110 were used to just test increasing the values and making sure they output correctly as well as mixing in some numbers that end with zero, one, and ten.

$1050 was used to confirm that 050 is treated as 50 and not cut into 05 when being a sub 100 number.

'' and $1e2 are used to test empty number and scientific notation.

And finally, a number was used that is bigger than supported, to confirm the output to user confirms that it is too large to generate.

### Brower Testing
As far as browser testing, I've tested this on Windows 10 with Edge, Chrome, and Firefox. And on Android I've tested it on Chrome. Anything else is untested as I don't currently have access to them.

IE11 has some styling/layout issues, but it's 'good enough' for a deprecated browser. If it was a requirement for this to be supported I'd put in more effort for it.

## Limitations
The maximum number can only be 123 digits long (excluding minus sign), and the same length for negative numbers. This is purely because the page I used to get the names for the larger numbers only went this large. If you know each word after Quadragintillion you can add it to the LARGE_NUMBERS array in `./src/static/numberToWords.js` and numbers up to that size should be supported. Going over this number will result in an error message to the user telling them the number isn't supported.

Scientific notation (1e2 etc) also isn't supported, and the user will be notified of this if they try to provide a number that is formatted in this way. 

It's worth mentioning yet again that the 'database' is just a json file, so there's some obvious scaling limitations there compared to a real database.

There's also some UX stuff that hasn't been implemented, such as loading indicators while submitting credentials, and there's no frontend logic checking for username/password. Usernames and passwords also have no requirement other than being at least one character long (and username can't already exist when registering a new account).

There's also no password reset, and no welcome emails or anything.

I also haven't set up anything like eslint to format the code so it's consistant throughout.